import React from 'react';
import Banner from '../components/Banner';
import Head from 'next/head';



export default function Home() {

    const data = {
        title: "Budget Monitor Application",
        destination:"/",
    }

    return (
        <React.Fragment>
            <Head>
                <title>Home</title>
            </Head>
            <Banner data={data}/>
        </React.Fragment>
    )
}