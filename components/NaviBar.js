import React, { useContext } from 'react';
import Link from 'next/link';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';
import localStorage from 'localStorage'

export default function NaviBar() {
	//const {user} = useContext(UserContext);

	return (
	<Navbar bg="info" variant="dark" expand="lg">
	  	<Link href="/">
              <a className="navbar-brand">Budget Monitor</a>
        </Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
                <Link href="/">
                    <a className="nav-link" role="button">Home</a>
                </Link>
			  {(localStorage.getItem('token') !== null) ?
			  	<React.Fragment>
			  	<Link href="/budgetTracker">
				  <a className="nav-link" role="button">Tracker</a>
			  	</Link>
				  <Link href="/chart">
                    <a className="nav-link" role="button">Pie Chart</a>
                </Link>
				<Link href="/trend">
                    <a className="nav-link" role="button">Line Chart</a>
                </Link>
				<Link href="/logout">
                    <a className="nav-link" role="button">Logout</a>
                </Link>
				</React.Fragment>
				:
				<React.Fragment>
					<Link href="/login">
                        <a className="nav-link" role="button">Login</a>
                    </Link>
					<Link href="/register">
                        <a className="nav-link" role="button">Register</a>
                    </Link>
				</React.Fragment>
			  } 
		    </Nav>
		</Navbar.Collapse>
	</Navbar>
	)
} 